import { TestBed, inject } from '@angular/core/testing';

import { Klaud9Service } from './klaud9.service';

describe('Klaud9Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Klaud9Service]
    });
  });

  it('should be created', inject([Klaud9Service], (service: Klaud9Service) => {
    expect(service).toBeTruthy();
  }));
});
