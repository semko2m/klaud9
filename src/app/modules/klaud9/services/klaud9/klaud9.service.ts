import {Injectable} from '@angular/core';
import {HttpClientService} from "../../../shell/services/http-client.service";

@Injectable()
export class Klaud9Service {

  constructor(private httpClientService: HttpClientService) {
  }

  /**
   * Get public IP
   */
  getPublicIp() {
    const url = 'https://api.ipify.org?format=json';
    return this.httpClientService.get(url, null, false)
  }
}
