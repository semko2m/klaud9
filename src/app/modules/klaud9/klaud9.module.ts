import {ModuleWithProviders, NgModule} from '@angular/core';

import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProgressbarModule, TypeaheadModule} from 'ngx-bootstrap';
import {DataService} from '../shell/services/data.service';
import {Klaud9Service} from "./services/klaud9/klaud9.service";


import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {Hello1Component} from './components/hello1/hello1.component';
import {Hello2Component} from './components/hello2/hello2.component';


@NgModule({
  declarations: [
    PageNotFoundComponent,
    HomeComponent,
    Hello1Component,
    Hello2Component
  ],
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    TypeaheadModule.forRoot(),
    ProgressbarModule.forRoot(),
    RouterModule,
  ],
  exports: [
    HomeComponent,
  ]
})
export class Klaud9Module {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: Klaud9Module,
      providers: [DataService, Klaud9Service]
    };
  }
}
