import {Component, OnInit} from '@angular/core';
import {HttpClientService} from "../../../shell/services/http-client.service";
import {Klaud9Service} from "../../services/klaud9/klaud9.service";
import {Hello1} from "../../models/hello1";

@Component({
  selector: 'app-hello2',
  templateUrl: './hello2.component.html',
  styleUrls: ['./hello2.component.css']
})
export class Hello2Component implements OnInit {
  public dataHello1: Hello1;
  public publicIp;

  constructor(private klaud9Service: Klaud9Service) {
    this.dataHello1 = JSON.parse(localStorage.getItem('hello1'));

  }

  ngOnInit() {
    this.klaud9Service.getPublicIp().subscribe(response => {
      this.publicIp = response.data.items;
      console.log(this.publicIp);
    });
  }

}
