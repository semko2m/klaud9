import {Component, OnDestroy, OnInit} from '@angular/core';
import {Hello1} from "../../models/hello1";
import {FormStates} from "../../models/FormStates";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-hello1',
  templateUrl: './hello1.component.html',
  styleUrls: ['./hello1.component.scss']
})
export class Hello1Component implements OnInit, OnDestroy {

  formModel: Hello1;
  formStates = FormStates;
  formState = FormStates.Add;
  formSubmitAttempt: boolean;

  name = new FormControl(null, [Validators.required, Validators.minLength(3)]);
  surname = new FormControl(null, [Validators.required]);
  email = new FormControl(null, [Validators.required]);

  formGroup = new FormGroup({
    name: this.name,
    surname: this.surname,
    email: this.email
  });

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.formModel = null;
    this.formGroup = null;
    this.formState = FormStates.View;
    this.formSubmitAttempt = null;
    this.name = null;
    this.surname = null;
    this.email = null;
  }


  enableForm() {
    this.formGroup.enable();
  }

  disableForm() {
    this.formGroup.disable();
  }

  setFormValues() {
    // console.log('this.formModel', this.formModel);
    this.formGroup.patchValue(this.formModel, {onlySelf: true});
  }


  isValidForm() {
    return this.formGroup.valid;
  }

  onReset() {
    this.formGroup.reset();
    this.formGroup.setValue(this.formModel, {onlySelf: true});
    this.formSubmitAttempt = false;
    this.setFormValues();
  }

  onCancel() {
    this.setFormState(FormStates.View);
    this.onReset();
    this.disableForm();
  }

  onEdit() {
    this.setFormState(FormStates.Edit);
    this.enableForm();
  }

  setFormState(state) {
    this.formState = state;
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        control.controls.forEach(c => this.markFormGroupTouched(c));
      }
    });
  }

  onSubmit() {
    this.formSubmitAttempt = true;
    this.markFormGroupTouched(this.formGroup);
    if (this.isValidForm()) {
      if (this.formState === FormStates.Edit) {
        // this.update(this.id, this.formGroup.value);
      } else {
        this.create(this.formGroup.value);
      }

    } else {
      console.log('form Is Not valid');
    }
  }

  update(id, payload) {

  }

  /**
   * Create function will send data to next component.
   * We will store data in localstorage
   */
  create(payload) {
    console.log(payload);
    localStorage.setItem('hello1', JSON.stringify(payload));
    this.router.navigate(['/hello2']);
  }
}
