/**
 * ResponseWrapper class
 */
export class ResponseWrapper {

  private _status: string;
  private _message: string;
  private _data: any;

  /**
   * Parse json to object
   * @param json
   * @returns {ResponseWrapper}
   */
  public static parse( json ) {
    return new this(json);
  }

  /**
   * Constructor
   * @param json
   */
  constructor( json ) {
    if (typeof json !== 'undefined' && json.status !== 'undefined') {
      this.status = json.status;
    }

    if (typeof json !== 'undefined' && json.message !== 'undefined') {
      this.message = json.message;
    }

    if (typeof json !== 'undefined' && json.data !== 'undefined') {
      this.data = json.data;
    }

    // To be removed after standardising api response
    if (typeof json !== 'undefined') {
      // console.log('ITEMS', json);
      this.status = 'success';
      this.data = {
        items: json
      };
      this.message = 'Successful operation';
    }
  }

  /**
   * Get state
   * @returns {string}
   */
  get status(): string {
    return this._status;
  }

  /**
   * Set state
   * @param value
   */
  set status( value: string ) {
    this._status = value;
  }

  /**
   * Get confirmMessage
   * @returns {string}
   */
  get message(): string {
    return this._message;
  }

  /**
   * Set confirmMessage
   * @param value
   */
  set message( value ) {
    this._message = value;
  }

  /**
   * Get data
   * @returns {any}
   */
  get data(): any {
    return this._data;
  }

  /**
   * Set data
   * @param value
   */
  set data( value ) {
    this._data = value;
  }

}
