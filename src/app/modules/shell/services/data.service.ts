import { Injectable } from '@angular/core';
import { SharedData } from '../model/shared-data';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {

  // Observable string sources
  private emitChangeSource = new Subject();

  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();

  emitChange( entity: string, action: string, data?: any, message?: any ) {
    const sharedData: SharedData = {
      entity: entity,
      action: action,
      data: data,
      message: message
    };
    this.emitChangeSource.next(sharedData);
  }

}
