import { Injectable } from '@angular/core';
import { Http, RequestOptions, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { ResponseWrapper } from '../class/response-wrapper';
import { ResponseHelper } from '../helpers/response-helper';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
 * TODO: Implement notification service
 */
@Injectable()
export class HttpClientService {

  /**
   * Request header data.
   */
  private headers: Headers;

  constructor( private http: Http,
               private router: Router ) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    // this.setAuthorizationHeader(this._cookieService.get('token'));
  }

  /**
   * Set json web token (JWT) authorization header.
   * Make sure we set Authorization header only once (avoid exceptions).
   * @param token
   */
  setAuthorizationHeader( token: string ) {
    this.headers.delete('Authorization');
    this.headers.append('Authorization', 'Bearer ' + token);
  }

  /**
   * Http GET Method with Response Wrapper.
   * @param {string} url
   * @param {RequestOptionsArgs} options
   * @param {boolean} alert
   * @returns {Observable<ResponseWrapper>}
   */
  get( url: string, options?: RequestOptionsArgs, alert?: boolean ): Observable<ResponseWrapper> {
    if (options == null) {
      options = new RequestOptions();
    }
    // append predefined options
    options.headers = this.headers;
    return this.http.get(url, options)
      .map(( res: Response ) => {
        const result = ResponseWrapper.parse(res.json());
        if (!this._isInvalidToken(result)) {
          return result;
        }
      })
      .catch(( error: any ) => {
        if (alert) {
          // this.alertNotificationService.error(error.status + ' ' + error.statusText, error.url);
        }
        return Observable.throw(error || 'Server error');
      });
  }

  /**
   * Http POST Method with Response Wrapper.
   * @param {string} url
   * @param {object} body
   * @param {RequestOptionsArgs} options
   * @param {boolean} alert
   * @returns {Observable<ResponseWrapper>}
   */
  post( url: string, body: object, options?: RequestOptionsArgs, alert?: boolean ): Observable<ResponseWrapper> {
    if (options == null) {
      options = new RequestOptions();
    }
    // append predefined options
    options.headers = this.headers;

    return this.http.post(url, body, options)
      .map(( res: Response ) => {
        const result = ResponseWrapper.parse(res.json());
        if (!this._isInvalidToken(result)) {
          return result;
        }
      })
      .catch(( error: any ) => {
        if (alert) {
          // this.alertNotificationService.error(error.status + ' ' + error.statusText, error.url);
        }
        return Observable.throw(error || 'Server error');
      });
  }

  /**
   * Http PUT Method with Response Wrapper.
   * @param {string} url
   * @param {object} body
   * @param {RequestOptionsArgs} options
   * @param {boolean} alert
   * @returns {Observable<ResponseWrapper>}
   */
  put( url: string, body: object, options?: RequestOptionsArgs, alert?: boolean ): Observable<ResponseWrapper> {
    if (options == null) {
      options = new RequestOptions();
    }
    // append predefined options
    options.headers = this.headers;

    return this.http.put(url, body, options)
      .map(( res: Response ) => {
        const result = ResponseWrapper.parse(res.json());
        if (!this._isInvalidToken(result)) {
          return result;
        }
      })
      .catch(( error: any ) => {
        if (alert) {
          // this.alertNotificationService.error(error.status + ' ' + error.statusText, error.url);
        }
        return Observable.throw(error || 'Server error');
      });
  }

  /**
   * Http DELETE Method with Response Wrapper.
   * @param url
   * @param options
   * @returns {Observable<R|T>}
   */
  delete( url: string, options?: RequestOptionsArgs, alert?: boolean ): Observable<ResponseWrapper> {
    if (options == null) {
      options = new RequestOptions();
    }
    // append predefined options
    options.headers = this.headers;

    return this.http.delete(url, options)
      .map(( res: Response ) => {
        const result = ResponseWrapper.parse(res.json());
        if (!this._isInvalidToken(result)) {
          return result;
        }
      })
      .catch(( error: any ) => {
        if (alert) {
          // this.alertNotificationService.error(error.status + ' ' + error.statusText, error.url);
        }
        return Observable.throw(error || 'Server error');
      });
  }


  _isInvalidToken( result ) {
    if (result.status === ResponseHelper.INVALID_TOKEN) {
      // this.authenticationService.logout();
      // this.alertNotificationService.error(result.message, 'You will be redirected to login.');
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 2000);
      return true;
    } else {
      return false;
    }
  }

}
