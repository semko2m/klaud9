export class ResponseHelper {

  /**
   * Success magic string.
   * @type {string}
   */
  static readonly SUCCESS = 'success';
  static readonly ERROR = 'error';
  static readonly INVALID_REQUEST = 'invalidrequest';
  static readonly INVALID_TOKEN = 'invalidtoken';
  static readonly UNAUTHORIZED = 'unauthorized';
}
