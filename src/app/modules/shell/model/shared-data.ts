export interface SharedData {
  entity?: string;
  action: string;
  message?: string;
  data?: object;
}
