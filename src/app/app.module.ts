import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {Routing} from './config/app-routes';
import {Klaud9Module} from './modules/klaud9/klaud9.module';
import {HttpClientService} from "./modules/shell/services/http-client.service";


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    Routing,
    Klaud9Module.forRoot(),
  ],
  providers: [HttpClientService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
