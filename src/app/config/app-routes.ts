import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from '../modules/klaud9/components/page-not-found/page-not-found.component';
import {HomeComponent} from "../modules/klaud9/components/home/home.component";
import {Hello1Component} from "../modules/klaud9/components/hello1/hello1.component";
import {Hello2Component} from "../modules/klaud9/components/hello2/hello2.component";

export const AppRoutes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'hello1', component: Hello1Component},
  {path: 'hello2', component: Hello2Component},
  {path: '**', component: PageNotFoundComponent}
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(AppRoutes, {enableTracing: false});
