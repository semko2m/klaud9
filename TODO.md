- Create public git repository (github, bitbucket…)
- Publish README.md with the content: “Hello Klaud9”
- Create first commit
- Create new branch with name “devel”, use this for develop
- Create a basic app with angular v4
- Create second commit
- Create the next routes:
- / this page contains 1 link pointing at /hello1
- /hello1
this page contains a form with name, surname, email and submit button, this submit button send to hello2 page
- /hello2
show the hello1 form information in a table and retrieve your IP address using some public REST API
- Finally merge devel to master
- Send us your github link

Plus points:
- Use bootstrap
- Use models for the form
- Use RxJS to do API Call
